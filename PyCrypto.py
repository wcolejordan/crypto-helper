import argparse
import multiprocessing
import signal
import time

from GetData import GetData


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def transferArrived(exchange, market, num):
    get = GetData()
    print(str(get.checkBalance(exchange, market)) + " " + str(num))
    return

def getPercent(market, num):
    get = GetData()
    var = get.getarbpercent(market)
    print("{:>12}{:>40}{:>10}".format("%.4f" % var[0] + "%", var[1], str(num)))

def checkDifTick(market, num):
    get = GetData()
    get.checkdif(market, str(num))

def getPortfolioVal():
    get = GetData()
    get.portfolioVal()

if __name__ == '__main__':
    jobs = []
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-m", "--market", help="market to compare e.g. DGB")
    group.add_argument("-v", "--value", help="shows portfolio information and values - must be used alone",
                       action="store_true")
    parser.add_argument("-p", "--percent", help="add this flag to only display the possible percent gain and exchange to sell on", action="store_true")

    args = parser.parse_args()
    if args.market:
            pool = multiprocessing.Pool(2, init_worker)
            try:
                if args.percent:
                    i = 1
                    while True:
                        pool.apply_async(getPercent, ([(args.market, i),("DGB", i)]))
                        i = i + 1
                        time.sleep(1)
                else:
                    i = 1
                    while True:
                        pool.apply_async(checkDifTick, ([args.market, i]))
                        i = i + 1
                        time.sleep(1)

            except KeyboardInterrupt:
                pool.close()
                pool.join()


    elif args.value:
        getPortfolioVal()
    else:
        print("Enter PyCrypto.py -h or --help for info")

