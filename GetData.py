import json
import os

from APIKeys import APIKeys
from Wrappers import poloniex, bittrex


class GetData():

    def __init__(self):
        self.p1 = poloniex(APIKeys.poloAPI, APIKeys.poloSecret)
        self.b1 = bittrex(APIKeys.bittrexAPI, APIKeys.bittrexSecret)

    def getarbpercent(self, market):

        bittrexMarket = "BTC-" + market
        poloMarket = "BTC_" + market
        bittrexTick = self.b1.returnTicker(bittrexMarket)
        poloTick = self.p1.returnTicker()
        bittrexAsk = bittrexTick["result"]["Ask"]
        bittrexBid = bittrexTick["result"]["Bid"]

        poloAsk = float(poloTick[poloMarket]["lowestAsk"])
        poloBid = float(poloTick[poloMarket]["highestBid"])

        percentageBittrex = (bittrexBid / poloAsk) * 100 - 100
        percentagePolo = (poloBid / bittrexAsk) * 100 - 100

        if percentageBittrex > percentagePolo:
            return percentageBittrex, "Poloniex [BUY] -> Bittrex [SELL]"
        elif percentagePolo > percentageBittrex:
            return percentagePolo, "Bittrex [BUY] -> Poloniex [SELL]"
        else:
            return 0, "Equal"


    def checkdif(self, market, num=""):

        bittrexMarket = "BTC-" + market
        poloMarket = "BTC_" + market

        bittrexTick = self.b1.returnTicker(bittrexMarket)
        poloTick = self.p1.returnTicker()

        bittrexAsk = bittrexTick["result"]["Ask"]
        bittrexBid = bittrexTick["result"]["Bid"]
        bittrexLast = bittrexTick["result"]["Last"]

        poloAsk = float(poloTick[poloMarket]["lowestAsk"])
        poloBid = float(poloTick[poloMarket]["highestBid"])
        poloLast = float(poloTick[poloMarket]["last"])

        percentageBittrex = (bittrexBid / poloAsk) * 100 - 100
        percentagePolo = (poloBid / bittrexAsk) * 100 - 100

        formattedOutput = "{:<10}{:^20}{:^20} ".format(num + " " + bittrexMarket, "Bittrex", "Poloniex") + \
            "\n{:<10}{:^20.8f}{:^20.8f}".format("Ask", bittrexAsk, poloAsk) + \
            "\n{:<10}{:^20.8f}{:^20.8f}".format("Bid", bittrexBid, poloBid) + \
            "\n{:<10}{:^20.8f}{:^20.8f}".format("Last", bittrexLast, poloLast) + \
            "\n{:<10}{:^20.8f}{:^20.8f}".format("B-S X", (bittrexBid - poloAsk),(poloBid - poloAsk)) + \
            "\n{:<10}{:^20.4f}{:^20.4f}\n".format("B/S %", percentageBittrex, percentagePolo)

        os.system('cls' if os.name == 'nt' else 'clear')
        print(formattedOutput)

    def getBTCVal(self):
        return float(self.p1.returnTicker()["USDT_BTC"]["last"])

    def getRates(self, market):
        try:
            polo = float(self.p1.returnTicker()["BTC_" + market]["last"])
        except:
            polo = float(0)

        try:
            bit = float(self.b1.returnTicker("BTC-" + market)["result"]["Last"])
        except:
            bit = float(0)
        return polo, bit

    def portfolioVal(self):
        name = ""
        valBTC = 0
        valUSD = 0
        balance = 0
        rate = 0
        rateBTC = self.getBTCVal()
        layout = "{:<10}{:<30}{:<30}{:<30}{:<30}"

        store = self.b1.returnBalances()["result"]
        poloniexJson = self.p1.returnBalances()
        bittrexJson = {}
        for i in range(0, len(store)):
            bittrexJson.update(
                {
                    store[i]["Currency"]: store[i]["Balance"]
                }
            )

        arr = [poloniexJson, bittrexJson]
        finalTotal = 0
        for i in range(0,2):
            total = 0
            if i == 0:
                print("-- Poloniex ------------------------------------------------------------------------------------------------------")
            else:
                print("-- Bittrex -------------------------------------------------------------------------------------------------------")
            print(layout.format("Market", "Balance", "BTC Rate", "BTC Equivalent", "USD Equivalent"))
            for name, bal in arr[i].items():
                balance = float(bal)
                rate = 1
                if balance > 0:
                    if name != "BTC":
                        rate = self.getRates(name)[i]
                        valBTC = (rate * balance)
                        total = total + valBTC
                    else:
                        valBTC = balance
                        total = total + balance
                    valUSD = valBTC * rateBTC
                    print(layout.format(name, '%.8f' % balance, '%.8f' % rate, '%.8f' % valBTC, '%.8f' % valUSD))
            print(layout.format("*TOTAL*", "x", "x", '%.8f' % total, '%.8f' % (total * rateBTC)))
            print()
            finalTotal = finalTotal + total
        totalUSD = finalTotal * rateBTC
        print("Estimate BTC Value of Portfolio: " + '%.8f' % finalTotal)
        print("Estimate USD Value of Portfolio: $" + '%.2f' % totalUSD)
        print("..................................................................................................................")
        print("Current BTC Value:               $" + '%.2f' % rateBTC)
        print("..................................................................................................................")


    def checkBalance(self, exchange, market):
        try:
            if exchange == "Poloniex":
                return float(self.p1.returnBalances()[market])
            elif exchange == "Bittrex":
                return self.b1.returnBalance(market)["result"]["Balance"]
        except:
            return 0;


if __name__ == '__main__':
    get = GetData()
    get.portfolioVal()





