from datetime import timedelta

from dateutil import rrule
from pandas import pandas


class Indicators:

    def __init__(self, df):
        self.p = pandas
        self.df = df

    def createDataSet(self, currentdt, period, range, types="Minutely", candlestick="Open"):
        rule = 5
        currentdt = self.p.to_datetime(currentdt)
        if types == "Hourly":
            rule = 4
            enddt = currentdt + timedelta(hours=period * (range - 1))
        elif types == "Daily":
            rule = 3
            enddt = currentdt + timedelta(days=period * (range - 1))
        else:
            enddt = currentdt + timedelta(minutes=period * (range - 1))
        collectedData = self.p.DataFrame([])
        for dt in rrule.rrule(rule, interval=period, dtstart=currentdt, until=enddt):
            endLoop = False
            dt = self.p.to_datetime(dt)
            lockout = 0
            while not endLoop:
                try:
                    collectedData = collectedData.append(self.p.DataFrame({'TimeStamp': dt, candlestick: self.df.ix[dt][candlestick]}, index=[0]), ignore_index=True)
                    endLoop = True

                except KeyError:
                    dt += timedelta(minutes=1)
                    lockout += 1
                    if dt > self.p.to_datetime("2017-06-25 22:58:00") or lockout > 100:
                        print("Inconsistent Dates")
                        collectedData = collectedData.append(self.p.DataFrame({'TimeStamp': dt, candlestick: 'NaN'}, index=[0]),ignore_index=True)
                        endLoop = True
        collectedData.set_index(['TimeStamp'], inplace=True)
        return collectedData


    def movingAvg(self, collectedData, frames, candlestick):
        columnname = 'MovingAverage' + str(frames)
        collectedData[columnname] = collectedData[candlestick].rolling(window=frames).mean()
        return collectedData
