import hashlib
import hmac
import json
import time
import urllib
from urllib.request import urlopen

class bittrex:
    def __init__(self, APIKey, Secret):
        self.__APIKey = APIKey
        self.__Secret = Secret
        self.__API_URL = 'https://bittrex.com/api/v1.1/'

    def __api_query(self, command, options=''):
        try:
            if (command == "getmarkets" or command == "getcurrencies" or command == "getmarketsummaries" or command == "getticker" or command == "getmarketsummary" or command == "getorderbook" or command == "getmarkethistory"):
                ret = urlopen(self.__API_URL + 'public/' + command + options)
                return json.loads(ret.read())
            else:
                uri = self.__API_URL + command
                sign = hmac.new(bytes(self.__Secret.encode("utf-8")), bytes(uri.encode("utf-8")),
                                hashlib.sha512).hexdigest()
                headers = {
                    'apisign': sign
                }
                ret = urlopen(urllib.request.Request(uri, headers=headers))
                return json.loads(ret.read())
        except(Exception, KeyboardInterrupt):
            #print(Exception or KeyboardInterrupt)
            return

            # ---- Public ---- #

    def returnMarkets(self):
        return self.__api_query('getmarkets')

    def returnCurrencies(self):
        return self.__api_query('getcurrencies')

    def returnMarketSummaries(self):
        return self.__api_query('getmarketsummaries')

    def returnTicker(self, market):
        return self.__api_query('getticker', '?market=' + market)

    def returnMarketSummary(self, market):
        return self.__api_query('getmarketsummary', '?market=' + market)

    def returnOrderBook(self, market, ordertype, depth):
        return self.__api_query('getorderbook', '?market=' + market + '&type=' + ordertype + '&depth=' + depth)

    def returnMarketHistory(self, market):
        return self.__api_query('getmarkethistory', '?market=' + market)

    # ---- Private ---- #

    def returnBalances(self):
        return self.__api_query('account/getbalances?apikey=' + self.__APIKey + '&nonce=' + str(time.time()))

    def returnBalance(self, currency):
        return self.__api_query(
            'account/getbalance?apikey=' + self.__APIKey + '&nonce=' + str(time.time()) + '&currency=' + currency)

    def returnOrders(self):
        return self.__api_query('market/getopenorders?apikey=' + self.__APIKey + '&nonce=' + str(time.time()))

class poloniex:
    def __init__(self, APIKey, Secret):
        self.__APIKey = bytes(APIKey.encode('utf-8'))
        self.__Secret = bytes(Secret.encode('utf-8'))

    def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
        return time.mktime(time.strptime(datestr, format))

    def __post_process(self, before):
        after = before

        # Add timestamps if there isnt one but is a datetime
        if ('return' in after):
            if (isinstance(after['return'], list)):
                for x in range(0, len(after['return'])):
                    if (isinstance(after['return'][x], dict)):
                        if ('datetime' in after['return'][x] and 'timestamp' not in after['return'][x]):
                            after['return'][x]['timestamp'] = float(
                                self.createTimeStamp(after['return'][x]['datetime']))

        return after

    # noinspection
    def __api_query(self, command, req={}):
        try:
            if command == "returnTicker" or command == "return24Volume":
                ret = urlopen('https://poloniex.com/public?command=' + command)
                return json.loads(ret.read())
            elif command == "returnOrderBook":
                ret = urlopen(
                    'https://poloniex.com/public?command=' + command + '&currencyPair=' + str(req['currencyPair']))
                return json.loads(ret.read())
            elif command == "returnMarketTradeHistory":
                ret = urlopen('https://poloniex.com/public?command=' + "returnTradeHistory" + '&currencyPair=' + str(
                    req['currencyPair']))
                return json.loads(ret.getvalue())
            req['nonce'] = int(time.time() * 1000)
            req['command'] = command
            # noinspection PyUnresolvedReferences
            post_data = bytes(urllib.parse.urlencode(req).encode("utf-8"))

            sign = hmac.new(self.__Secret, post_data, hashlib.sha512).hexdigest()
            headers = {
                'Sign': sign,
                'Key': self.__APIKey
            }
            ret = urlopen(urllib.request.Request('https://poloniex.com/tradingApi', post_data, headers))
            jsonRet = json.loads(ret.read())
            return self.__post_process(jsonRet)
        except(Exception, KeyboardInterrupt):
            #print(Exception or KeyboardInterrupt)
            return

    def returnTicker(self):
        return self.__api_query("returnTicker")

    def return24Volume(self):
        return self.__api_query("return24Volume")

    def returnOrderBook(self, currencyPair):
        return self.__api_query("returnOrderBook", {'currencyPair': currencyPair})

    def returnMarketTradeHistory(self, currencyPair):
        return self.__api_query("returnMarketTradeHistory", {'currencyPair': currencyPair})

    # Returns all of your balances.
    # Outputs:
    # {"BTC":"0.59098578","LTC":"3.31117268", ... }
    def returnBalances(self):
        return self.__api_query('returnBalances')

    # Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP"
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # orderNumber   The order number
    # type          sell or buy
    # rate          Price the order is selling or buying at
    # Amount        Quantity of order
    # total         Total value of order (price * quantity)
    def returnOpenOrders(self, currencyPair):
        return self.__api_query('returnOpenOrders', {"currencyPair": currencyPair})

    # Returns your trade history for a given market, specified by the "currencyPair" POST parameter
    # Inputs:
    # currencyPair  The currency pair e.g. "BTC_XCP"
    # Outputs:
    # date          Date in the form: "2014-02-19 03:44:59"
    # rate          Price the order is selling or buying at
    # amount        Quantity of order
    # total         Total value of order (price * quantity)
    # type          sell or buy
    def returnTradeHistory(self, currencyPair):
        return self.__api_query('returnTradeHistory', {"currencyPair": currencyPair})

    # Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is buying at
    # amount        Amount of coins to buy
    # Outputs:
    # orderNumber   The order number
    def buy(self, currencyPair, rate, amount):
        return self.__api_query('buy', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
    # Inputs:
    # currencyPair  The curreny pair
    # rate          price the order is selling at
    # amount        Amount of coins to sell
    # Outputs:
    # orderNumber   The order number
    def sell(self, currencyPair, rate, amount):
        return self.__api_query('sell', {"currencyPair": currencyPair, "rate": rate, "amount": amount})

    # Cancels an order you have placed in a given market. Required POST parameters are "currencyPair" and "orderNumber".
    # Inputs:
    # currencyPair  The curreny pair
    # orderNumber   The order number to cancel
    # Outputs:
    # succes        1 or 0
    def cancel(self, currencyPair, orderNumber):
        return self.__api_query('cancelOrder', {"currencyPair": currencyPair, "orderNumber": orderNumber})

    # Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method, the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount", and "address". Sample output: {"response":"Withdrew 2398 NXT."}
    # Inputs:
    # currency      The currency to withdraw
    # amount        The amount of this coin to withdraw
    # address       The withdrawal address
    # Outputs:
    # response      Text containing message about the withdrawal
    def withdraw(self, currency, amount, address):
        return self.__api_query('withdraw', {"currency": currency, "amount": amount, "address": address})

