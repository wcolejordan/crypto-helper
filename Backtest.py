from datetime import timedelta

from pandas import pandas

from Indicators import Indicators


class Backtest(Indicators):

    def __init__(self, market):
        print("-- Loading Data --")
        self.p = pandas
        self.df = self.p.read_csv(
            "Bittrex Market History 6-25-2017/6-25-2017/" + market + ".csv",skiprows=1)
        self.df.rename(columns=lambda x: x.strip().replace('[','').replace(']',''), inplace=True)
        self.df['TimeStamp'] = self.p.to_datetime(self.df['TimeStamp'])
        self.df.set_index(['TimeStamp'], inplace=True)
        print("-- Data Loaded --")

    def getSafeDate(self, period, frame, type='Minutely'):
        timestamp = self.df.ix[0].name
        if type == "Hourly":
            timestamp += timedelta(hours=period*frame)
        elif type == "Daily":
            timestamp += timedelta(days=period*frame)
        else:
            timestamp += timedelta(minutes=period*frame)
        return timestamp

    def nextDateMins(self, date, mins):
        return date + timedelta(minutes=mins)

    def nextDateHours(self, date, hours):
        return date + timedelta(hours=hours)

    def nextDateDays(self, date, days):
        return date + timedelta(days=days)

    def getFirstDate(self):
        return self.df.ix[0].name
