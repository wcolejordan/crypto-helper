import sched
import time

from AlphaModel import AlphaModel


class Schedule:

    def __init__(self):
        self.s = sched.scheduler(time.time, time.sleep)
        self.alpha = AlphaModel
        self.counter = 0
        return


    def run(self):
        time.sleep(2)
        self.alpha.scope(self)


    def loop(self):
        while(True):
            self.run()

if __name__ == '__main__':
    schedule = Schedule()
    #schedule.loop()
    schedule.alpha.backtest(schedule.alpha)





