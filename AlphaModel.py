import time
import pandas
import matplotlib.pyplot as plot
import matplotlib.ticker as ticker
from Backtest import Backtest


class AlphaModel:

    def __init__(self):
        self.counter = 0

    def backtest(self):
        bt = Backtest("BTC-ETH")
        p = pandas
        period = 1000
        avgrange = 100
        avg1 = 10
        avg2 = 30
        avg3 = 50
        date = bt.getFirstDate()
        candlestick = 'Open'
        bought = 0
        sold = 0
        btcbal = 5.0

        df = bt.createDataSet("2017-01-19 21:00:00", period, avgrange, candlestick=candlestick)
        bt.movingAvg(p.DataFrame(df), avg1, candlestick)
        bt.movingAvg(p.DataFrame(df), avg2, candlestick)
        bt.movingAvg(p.DataFrame(df), avg3, candlestick)
        currentprice = 0
        for i in range(avg3-1, avgrange-1):
            currentprice = df.iloc[i][candlestick]
            if (df.iloc[i]['MovingAverage30'] > df.iloc[i]['MovingAverage50']) and bought == 0:
                bought = btcbal / currentprice
                btcbal = 0
                print('Bought', bought, currentprice)
            elif (df.iloc[i]['MovingAverage30'] < df.iloc[i]['MovingAverage50']) and bought > 0:
                btcbal = bought * currentprice
                bought = 0
                print('Sold', btcbal, currentprice)
        if bought > 0:
            btcbal = bought * currentprice
        print('Final', btcbal)

        ticklabels = df.index.strftime('%Y-%m-%d %H-%M-%S')
        plt = df.plot(xticks=ticklabels, kind='line')
        plt.grid()
        plt.xaxis.set_major_formatter(ticker.FixedFormatter(ticklabels))
        plot.show()


    def scope(self):
        #time.sleep(1)
        self.counter = self.counter + 1
        print("Running " + str(self.counter))
        if self.counter % 5 == 0:
            time.sleep(3)
